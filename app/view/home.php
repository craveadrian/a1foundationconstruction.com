<div id="about">
	<div class="row">
		<div class="panel fr">
			<div class="text">
				<h1>A-1 Affordable <span>Foundation & Construction</span></h1>
				<p>If you are looking for a local construction company that will do a great job for you no matter what task you have for us, you can’t go wrong with A-1 Affordable Foundation & Construction Co. With over 18 years’ experience, we have the skills and talent to take care of any job or task you need us to do. We are that excellent ‘contractor near me’ you often wished you knew! </p>
				<div class="panel-below">
					<div class="left">
						<a href="about#content" class="btn">LEARN MORE</a>
						<?php $this->info(["phone","tel","about-phone"]); ?>
					</div>
					<div class="right">
						<a href="https://www.bbb.com" target="_blank">
							<img src="public/images/common/bbb.png" alt="BBB" class="bbb">
						</a>
					</div>
				</div>
			</div>
			<div class="social-media">
				<a href="<?php $this->info("fb_link"); ?>" target="_blank">f</a>
				<a href="<?php $this->info("tt_link"); ?>" target="_blank">l</a>
				<a href="<?php $this->info("yt_link"); ?>" target="_blank">x</a>
				<a href="<?php $this->info("rs_link"); ?>" target="_blank">r</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="welcome">
	<h2>WELCOME</h2>
	<div class="row">
		<h3>No job too big or small</h3>

	</div>
</div>
